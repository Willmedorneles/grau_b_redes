﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChegadaControler : MonoBehaviour {

	public bool chegou;

	public bool pChegou
	{
		get { return pChegou; }
		set { chegou = value; }
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		

	}

	void OnTriggerEnter(Collider Collider){
		if (Collider.gameObject.name == "Player") {

			chegou = true;
			SceneManager.LoadScene("Selection");
		} 
		else
		{
			chegou = false;
		}
	}
}
