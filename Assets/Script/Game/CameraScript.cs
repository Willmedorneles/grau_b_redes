﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	GameObject player;

	float posx, posy;
	Vector3 speed = Vector3.zero;
	Vector3 playerLocal;

	Rigidbody rb;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		rb = player.GetComponent<Rigidbody> ();


	}
	
	// Update is called once per frame
	void Update () {
		
		playerLocal = new Vector3 (posx, posy, -10f);


		//speed = rb.velocity.magnitude;

		posy = player.transform.position.y;

		posx = player.transform.position.x;


		
		transform.position = Vector3.SmoothDamp (transform.position, playerLocal, ref speed, 0.2f);
			

	}
}
