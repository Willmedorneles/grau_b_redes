﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PLayerNetwork : NetworkBehaviour {

	[SerializeField] Camera cameraPlayer;
	[SerializeField] AudioListener audioListner;

	// Use this for initialization
	void Start () {
		if (isLocalPlayer) 
		{
			cameraPlayer.enabled = true;
			audioListner.enabled = true;
		}
	
	}


}
