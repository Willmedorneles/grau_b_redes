﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColorSelection : MonoBehaviour {


	protected GameObject button;
	protected GameObject display;
	protected Color colorSelected;

	public Color pColorSelected
	{
		get{ return colorSelected; }
		set{ colorSelected = value; }
	}

	// Use this for initialization
	void Start () {
		display = GameObject.Find ("PlayerColor");

	}


	public void changeColor(string buttonColor)
	{
		button = GameObject.Find (buttonColor);
		colorSelected = button.GetComponent<Image>().color;
		display.GetComponent<Image>().color = colorSelected;
	}

	public void quitGame()
	{
		Application.Quit ();
	}

	public void loadGame()
	{
		DontDestroyOnLoad (transform.gameObject);
		//PlayerPrefs.SetString ("playerColor", colorSelected);
		SceneManager.LoadScene("Reders");
	}
}
