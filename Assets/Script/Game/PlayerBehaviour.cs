﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerBehaviour : NetworkBehaviour {

	protected int jumpHight;
	protected float contadorInicial;
	protected float velocidade = 0f, velocidadeMax, time;
	protected bool rightKey, leftKey, combo, rodaContador, isGrounded;
	protected Rigidbody rb;
	protected Color playerColor;
	protected ColorSelection colorSelected;
	protected GameObject goColor;
	protected Material material;
	protected GameObject player;
	protected Renderer rendPlayer;


	public Vector3 pPosition
	{
		get{ return new Vector3(rb.position.x, rb.position.y, 0); }
		//set { }
	}

	// Use this for initialization
	void Start () {
		if (!isLocalPlayer) 
		{
			GameObject camera = this.gameObject.transform.GetChild (0).gameObject;

			camera.SetActive (false);
		}

		player = GameObject.Find("Player");
        velocidadeMax = 10f;
		jumpHight = 150;
        time = contadorInicial;
		rb = GetComponent<Rigidbody> ();
		goColor = GameObject.Find ("EventSystem");

		//rendPlayer = player.GetComponent<Renderer> ();
		//material.color = rendPlayer.material.color;

		//colorSelected = goColor.GetComponent<ColorSelection> ();

		//colorSelected.pColorSelected = rendPlayer.material.color;
		//player.GetComponent<Renderer> ().material.color = colorSelected.pColorSelected;

    }

	// Update is called once per frame
	void Update () {
		if (!isLocalPlayer) 
		{
			return;
		}


        detectaTecla();

		if (isGrounded == true && Input.GetKeyDown (KeyCode.UpArrow)){
			
			//transform.Translate(Vector3.up * 50 * Time.deltaTime, Space.World);
			rb.AddForce(new Vector2(0,jumpHight), ForceMode.Impulse);
			isGrounded = false;
		} 

		if (Input.GetKeyDown (KeyCode.DownArrow)){

			//transform.Translate(Vector3.up * 50 * Time.deltaTime, Space.World);
			rb.AddForce(new Vector2(0,-200), ForceMode.Impulse);
			isGrounded = false;
		} 



        if (!combo)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rightKey = false;
                leftKey = true;
                combo = true;
                rodaContador = true;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                leftKey = false;
                rightKey = true;
                combo = true;
                rodaContador = true;
            }

        }

        if(rodaContador)
        {
            if(time <= 0)
            {
                rodaContador = false;
                time = contadorInicial;
            }
            else
            {
                time -= Time.deltaTime;
            }
        }


        //transform.Translate(velocidade * Time.deltaTime, 0f, 0f);
        rb.AddForce(new Vector2(velocidade, 0), ForceMode.Impulse);
    }
    
	void OnCollisionEnter(Collision Collision){
		if(Collision.gameObject.name == "Ground")
		{
			isGrounded = true;
		}
	}

    protected void detectaTecla()
    {
		if(rightKey && Input.GetKey (KeyCode.LeftArrow))
        {
            leftKey = true;
            rightKey = false;
            time = contadorInicial;
            combo = true;
            if (velocidade < velocidadeMax)
            {
                velocidade +=1;
            }

        }
         if (leftKey && Input.GetKey(KeyCode.RightArrow))
        {
            leftKey = false;
			rightKey = true;
            time = contadorInicial;
            combo = true;
            if (velocidade < velocidadeMax)
            {
                velocidade +=1;
            }
        }

        else
        {
            if (velocidade > 0)
            {
                velocidade-= 0.1f;
            }
			if (velocidade < 0) 
			{
				velocidade = 0f;
			}
        }
    }
}
